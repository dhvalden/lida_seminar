# LIDA Cloud-based VM Seminar 19 Dec 2019

Hey everyone! and welcome to the LIDA Cloud-based VM Seminar 19 Dec 2019 Guide.
This is meant to be a very simple, step by step guide throughout the process of setting up your first virtual machine.
This guide will provide you with the commands and a brief explanation of the operation you are performing with each of them.
**This guide is only compatible with Bash and with Linux Ubuntu 18** although many commands are the same for other Linux distributions and shells.
Finally, this guide assumes you have administrator privileges over your system.

## Connecting to your virtual machine

During this tutorial you are going to work on a remote virtual machine. In order to connect to this remote machine you are going to use a secure shell, authenticated using a pair of keys.

First, open your terminal and run the following command:

```sh
ssh-keygen -t rsa
```

This command will create a pair of symmetric secret/public `ssh` keys in a directory called `/.shh` located  in your home directory. You must now go to this directory and open (using the text editor of your choice) the file ending with .pub which correspond to the public key of the pair. 
Now copy the entire content of the file. Be careful not to add trailing white spaces. Open GCP Console, navigate to your VMs site, choose your VM, click on **Edit**, and in the section of **SSH keys** select **Add Item**. In the blank box that just appeared paste the copied public key.
In order to connect to connect to your VM, you will need its address, which in this case is called **External IP**. Find it, and copy it.
Finally, go back to your terminal and run the following command, replacing the sections `user_name` and `ip_address` with your information.

```sh
ssh -i id_rsa user_name@ip_address
```

A few warnings will appear. Accept all of them. After this you should be connected to your Ubuntu remote Virtual Machine.

## Installing packages and tools in our Virtual Machine

These commands will install the tools and packages we are going to use in our tutorial. This pacakes are being installed in the VM, not in your computer.

First, lets update the package database

```sh
sudo apt-get update
```

Now, lets install some packages. We are going to use the flag `-y` to globally accept the dependencies for our packages

First, lets install Git

```sh
sudo apt-get install -y git
```

Now, Python3 and pip

```sh
sudo apt-get install -y python3
sudo apt-get install -y python3-pip
```

Now, lets install MongoDB. This requires some additional steps. Run these in order.

```sh
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
```
```sh
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
```
```sh
sudo apt-get update
```
```sh
sudo apt-get install -y mongodb-org
```

Once MongoDB is installed, we need to initialize it

```sh
sudo service mongod start
```

After this, you should be able to open the Mongo Shell by typing `mongo` and enter. You can quit the Mongo Shell by typing `exit` and enter.

Lets install also an example of SQL database server. PostgreSQL.

```sh
sudo apt-get install -y postgresql postgresql-contrib
```

After installation, in order to enter in the PostgreSQL shell you gonna have to switch user and then open the shell.

```sh
sudo su postgres
```

```sh
psql
```

To exit the shell and going back to your user shell, enter the following:

```sh
q
```

```sh
sudo su [yourUserName]
```

Now, lets install some necesary python libraries.

```sh
pip3 install pymongo pandas newsapi-python
```

These libraries will help us connect to the Google news API using Python, and to our Mongo database to store our collected news snippets.

Now, lets download this tutorial and its code to our virtual machine, so we can use it.
To achieve this, we are going to use the `git` command `clone` over this repository.

```sh
git clone https://gitlab.com/dhvalden/lida_seminar.git
```
This command will download and create a directory in your home directory. Check it using the `ls` command.

Finally, we will move the content of this new directory to your home directory with the following command

```sh
mv ~/lida_seminar/gnews ~/gnews
```

## Configuring the credentials of our application

The `/gnews` directory you just downloaded contains the following files:

*  A "keys" file, which should store your Google News API credentials.
*  A "keywords" file, which contains the keywords you will use to filter the news snippets you wish to collect.
*  And finally a python source file containing the code for our application.

For the sake of simplicity, the application we are going to use right now will look automatically in the keys and keywords files for the arguments it needs to run properly, but in future projects you may want to customize or even create your own application. I will also connect and dump every collected snippet to a mongo database called `googlenews`. The bits about configuring you database system are beyond the scope of this tutorial, mainly due to time constraints.

In order to start collecting snippets, we first need to setup our credentials. To do so, we just need to edit the file `keys` adding our Google News API credentials.

```sh
apikey=xxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

To edit it, use the following command which will open the file using the nano editor.

```sh
nano ~/gnews/keys
```

You can also add your own keywords to the `keywords` file using the same method.

```sh
nano ~/gnews/keywords
```
If you have done everything properly, we should be ready to test our application, using the following command.

```sh
python3 ~/gnews/googlenews_collector.py
```

## Automating the data collection 

You'll notice that by running the above command, your terminal will be locked. Also, if you close the connection, you application will be terminated. This is very inconvenient. To avoid that, we have several alternatives:

* We can set up a service, or "daemon", running constantly in the background in a reliable way.
* We can use the `nohup` command, which ignores the **H**ang **Up** signal sent to the process after closing the connection. This can be useful, but it is difficult to control.
* Finally, we can use `crontab` to schedule a recurrent job. This method is ideal for short running applications, like ours. **This is the method we are going to review**.

Enter the followwing command to open your contrab (scheduler):

```sh
crontab -e
```
In the screen you will see the instructions of how to use crontab. For now, lets schedule our program to run each day at midnight

```sh
@midnight /usr/bin/python3 /home/[userName]/gnews/googlenews_collector.py
```

## Managing our collected data

Rigth now, our data is being streamed and store into a MongoDB collection. This provides security and redundancy, as well as some utilities like search, spliting, and other management goodies. For now lets check how manmy snippets we have collected already.

Open mongo shell
```sh
mongo
```

List the available databases
```js
show dbs
```

Select your database
```js
use googlenews
```

List the available collections in your db
```js
show collections
```

Count how many documents have in your collection
```js
db.trump.count()
```

Take a look at the documents being stored
```js
db.trump.list()
```

Make a ramdom sample of 10 cases.

```js
db.trump.aggregate( [ { $sample: { size: 100 } }, { $out: "sample"} ])
```

Finally, exit the mongo shell typing `exit`.

This is all well and good, but what if I want to use this data for analysis. We need to export it to a more portable format.

```sh
mongoexport --db twts --collection sample --out sample.json
```

Check for the file in your home directory with `ls`.

If you want to download this file to your local machine, open a new terminal and there, issue the following command, replacing with your username and the IP of your virtual machine.

```sh
scp -i id_rsa user@IPaddress:~/sample.json ~/
```
This should download the sample.json file to your home directory in your local machine.

## Jupyter Notebooks

Jupyter notwbooks are a very useful tool for data analysis. They run in a server/client scheme, using the web browser as GUI for the client side, so it makes sense to install them in our VM.

First lets intall virtual environments.

```sh
pip3 install virtualenv
```

To run a jupyter notebook safely, we need to do it insede a `venv`.

```sh
mkdir myvenv
cd myvenv
python3 -m venv ./
```

Now, we need to active our newly created venv.

```sh
source bin/activate
```

Finally, inside this venv, we can install jupyter.

```sh
pip3 install jupyter
```

If everything went right, we can fire up a jupyter server:

```sh
jupyter notebook --no-browser --port=8888
```

Now, open a new terminal in your laptop, and enter the following command wich will allow you to tunnel the output from your VM 8888 port to your local 8888 port.

```sh
ssh -i id_rsa -N -L 8888:localhost:8888 user_name@ip_address
```

This command will only tunnel the output. To actually see the notebook go to http://localhost:8888 in your browser and follow the instructions.

## Kubernetes Clusters

TBA.
