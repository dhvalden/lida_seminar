#!/usr/bin/env python3

import sys
import os
import argparse
import pymongo
import datetime as date
from newsapi import NewsApiClient
from pymongo import MongoClient


def validate_file(file_name):
    if not os.path.exists(file_name):
        sys.exit("ERROR: The file %s does not exist" % file_name)


def read_keywords(file_name, save=False):
    with open(file_name, "r") as f:
        keywords = f.read().splitlines()
    if save:
        with open("keywords", "w") as o:
            for item in keywords:
                o.write("%s\n" % item)
    return keywords


def read_keys(file_name, key_names, save=False):
    credentials = {}
    with open(file_name, "r") as f:
        text = f.read()
        keylist = text.replace("=", " ").split()
    if save:
        with open("keys", "w") as o:
            o.write(text)
    for key in key_names:
        index = keylist.index(key)
        credentials[key] = keylist[index+1]
    return credentials


def collect(args, default_keywords):

    keywords_file = args.keywords_file
    keys_file = args.keys_file
    mongo_host = args.mongo_host
    key_names = args.key_names

    validate_file(keywords_file)
    validate_file(keys_file)

    if keywords_file == default_keywords:
        keywords = read_keywords(keywords_file)
        credentials = read_keys(keys_file, key_names)
    else:
        keywords = read_keywords(keywords_file, save=True)
        credentials = read_keys(keys_file, key_names, save=True)
    try:
        newsapi = NewsApiClient(api_key=credentials["apikey"])
        client = MongoClient(mongo_host)
        db = client.googlenews
        for word in keywords:
            articles = newsapi.get_everything(q=word, sort_by="relevancy")
            collection = pymongo.collection.Collection(db,
                                                       word.replace(" ", ""))
            for item in articles["articles"]:
                collection.update_many(item, {"$set": item},
                                       upsert=True)
    except Exception as e:
        now = date.datetime.now()
        print(e)
        print("date: " + now.strftime("%Y-%m-%d %H:%M:%S"))


def main():
    
    DEFAULT_MONGO_HOST = "mongodb://localhost/googlenews"
    DEFAULT_KEY_NAMES = ["apikey"]
    DEFAULT_PATH = os.path.dirname(os.path.realpath(__file__))
    DEFAULT_KEYWORDS = DEFAULT_PATH + "/keywords"
    DEFAULT_KEYS = DEFAULT_PATH + "/keys"

    parser = argparse.ArgumentParser(description="news_collector")
    parser.add_argument("keywords_file", type=str, nargs="?",
                        metavar="keywords_file", default=DEFAULT_KEYWORDS)
    parser.add_argument("keys_file", type=str, nargs="?", metavar="keys_file",
                        default=DEFAULT_KEYS)
    parser.add_argument("mongo_host", type=str, nargs="?", metavar="mongo_host",
                        default=DEFAULT_MONGO_HOST)
    parser.add_argument("key_names", type=str, nargs="?", metavar="key_names",
                        default=DEFAULT_KEY_NAMES)
    args = parser.parse_args()
    if args.keywords_file is not None and args.keys_file is not None:
        collect(args, DEFAULT_KEYWORDS)


if __name__ == "__main__":
    main()
